from flask import Flask
import assistant
app = Flask(__name__)

@app.route('/')
def hello_world():
    return '<h1 style="font-family: segoe ui">I\'m Yash</h1>'

@app.route('/test')
def test():
    a = assistant.assistant
    a.speak("Sir, your room temperature is greater than 29 degrees. Do you want me to turn on the AC?")
    return '<h1 style="font-family: segoe ui">Worked successfully</h1>'

if __name__ == '__main__':
    app.run(debug=True, host="192.168.1.28", port=8000)