import speech_recognition as sr
import pyttsx3
from colored import fg, bg, attr

yellow = fg('yellow')
yellowBg = bg('yellow')
green = fg('green')
greenBg = bg('green')
blue = fg('blue')
blueBg = bg('blue')
attri = attr(0)

class assistant:
  def listen():
    global query
    r = sr.Recognizer()
    with sr.Microphone() as source:
      print(f"{yellow}: listening...{attri}")
      r.pause_threshold = 1
      audio = r.listen(source)

    try :
      print(f"{yellow}: recognizing...{attri}")
      query = r.recognize_google(audio)
      print(f"User said: {green}{query}{attri}")
    except:
      return "None"
    return str(query)

  def speak(text):
    engine = pyttsx3.init('sapi5')
    voices = engine.getProperty('voices')
    engine.setProperty('voices',voices[1].id)
    engine.setProperty('rate',150)
    print(blue+text+attri)
    engine.say(text)
    engine.runAndWait()