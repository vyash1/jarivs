import os
from colored import fg, bg, attr
import speech_recognition as sr

yellow = fg('yellow')
green = fg('green')
attri = attr(0)

def takecommand():
  global query
  r = sr.Recognizer()
  with sr.Microphone() as source:
    print(f"{yellow}listening...{attri}")
    r.pause_threshold = 1
    audio = r.listen(source, phrase_time_limit=5)
  
  try :
    print(f"{yellow}recognizing...{attri}")
    query = r.recognize_google(audio)
    print(f"User said: {green}{query}{attri}")
  except:
    return "None"
  return query


while True:
  com = takecommand()
  if "wake up" in com:
    os.system("python main.py")