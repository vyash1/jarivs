from assistant import assistant
import datetime as dt
import time

#wish
def wish():
  hour = int(dt.datetime.now().hour)
  tt = time.strftime("%I:%M %p")
  if hour>=0 and hour<12:
    assistant.speak(f"Good morning sir, It's {tt}")
  elif hour>=12 and hour<15:
    assistant.speak(f"Good afternoon sir, It's {tt}")
  else:
    assistant.speak(f"Good evening sir, It's {tt}")
  assistant.speak(f"Tell me how could I help you.")

def current_time():
  tt = time.strftime("%I:%M %p")
  assistant.speak(f"It's {tt}")
