import coms
import os
from colored import fg, bg, attr
from assistant import assistant
import webbrowser
import keyboard

yellow = fg('yellow')
yellowBg = bg('yellow')
green = fg('green')
greenBg = bg('green')
blue = fg('blue')
blueBg = bg('blue')
attri = attr(0)

# coms.wish()

while True:
  query = assistant.listen()
  query = query.lower()

  if "go offline" in query:
    assistant.speak("System going offline.")
    break

  if "how are you" in query:
    assistant.speak("I'm good sir, here to help you any time")
  if "time" in query:
    coms.current_time()
  if "close this window" in query:
    keyboard.send('alt + F4')

  #Internals
  if "open files" in query:
    os.system("explorer.exe")
    assistant.speak('Sure sir, opening file explorer')
  if "open telegram" in query:
    path = "C:\\Users\\KondaiahAndSailaja\\OneDrive\\Desktop\\tportable-x64.2.7.4\\Telegram\\tgram.exe"
    os.startfile(path)
    assistant.speak('Sure sir, opening telegram')
  if "zoom" in query or "have a meeting" in query:
    path = "C:\\Users\\KondaiahAndSailaja\\AppData\\Roaming\\Zoom\\bin\\Zoom.exe"
    os.startfile(path)
    assistant.speak('Sure sir, opening Zoom meetings')
  if "cmd" in query:
    os.system("start cmd")
  if "open code" in query:
    os.system("code")
  if "open flask" in query:
    os.startfile("D:\\Programming\\Web\\Workspaces\\flask.code-workspace")


  #Web
  if "open my browser" in query:
    webbrowser.open("https://www.titaniumgoin.tk/")
  if "open my amazon" in query:
    webbrowser.open("https://www.amazon.in/gp/cart/view.html?ref_=nav_cart")
    assistant.speak('Sure sir, opening your Amazon cart')
  if "search google" in query:
    if "for" in query:
      query = query.split("for")[1]
      search = query.replace(' ','+')
      assistant.speak(f'Sure sir, searching google for {ser}')
    else:
      assistant.speak("What do you want me to search in Google")
      ser = assistant.listen()
      assistant.speak(f'Sure sir, searching google for {ser}')
      ser = ser.replace(' ','+')
    url = f"http://www.google.com/search?q={ser}"
    webbrowser.open(url)
  if "prime video" in query:
    assistant.speak(f"Opening prime.")
    url = "https://primevideo.com/"
    webbrowser.open(url)
  